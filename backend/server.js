const express = require("express");
const app = express();
const http = require("http");
const { Server } = require("socket.io");
const cors = require("cors");
app.use(cors());
const jwt = require("jsonwebtoken");

const path = require("path");

const server = http.createServer(app);

//on charge sequelize
const Sequelize = require("sequelize");

//On fabrique le lien de la bdd
const dbPath = path.resolve(__dirname, "chat.sqlite");

//on se connecte à la base
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

//on charge les models
const Chat = require("./src/Models/Chat.Model");
const Room = require("./src/Models/Room.Model");
const User = require("./src/Models/User.Model");
const UserRoom = require("./src/Models/UserRoom.Model");

const { createMessage } = require("./src/Services/Chat.Service");
const {
  createRoom,
  roomCreated,
  removeAllRooms,
  enterRoom,
  leaveRoom,
} = require("./src/Services/Room.Service");

const { inviteUser, displayInvitation, responseInvitation } = require("./src/Services/UserRoom.Service");

// On effectue le chargement
Chat.sync();
Room.sync();
User.sync();
UserRoom.sync();

const allowedOrigins = [
  "http://localhost:5173",
  "http://localhost:5174",
  "http://localhost:5500",
  "http://localhost:8080",
  "http://localhost:8081",
];

const io = new Server(server, {
  cors: {
    origin: allowedOrigins,
    methods: ["GET", "POST"],
  },
});

io.use(function (socket, next) {
  if (socket.handshake.query && socket.handshake.query.token) {
    jwt.verify(
      socket.handshake.query.token,
      "SECRET_KEY",
      async function (err, decoded) {
        if (err) return next(new Error("Authentication error"));
        socket.decoded = decoded;
        console.log(decoded);
        // On vérifie si l'utilisateur est présent dans la table User, s'il ne l'est pas, on l'ajoute
        const count = await User.count({
          where: {
            userId: decoded.id,
          },
        });
        if (count === 0) {
          User.create({
            userId: decoded.id,
          });
        }
        next();
      }
    );
  } else {
    next(new Error("Authentication error"));
  }
});
io.on("connection", (socket) => {
  console.log(`A user connected: ${socket.decoded.id}`);

  // partie chat
  createMessage(socket, io);

  // partie salon
  createRoom(socket, io);
  roomCreated(socket, io);
  removeAllRooms(socket, io);
  enterRoom(socket, io);
  leaveRoom(socket, io);

  // partie invitation
  inviteUser(socket, io);
  displayInvitation(socket, io);
  responseInvitation(socket, io)
});

server.listen(3000, () => {
  console.log("listening on *:3000");
});
