const { Model, DataTypes } = require("sequelize");
const Sequelize = require("sequelize");
const path = require("path");
const dbPath = path.resolve(__dirname, "chat.sqlite");
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

class User extends Model {}

User.init(
  {
    userId: { type: DataTypes.STRING, unique: true, allowNull: false },
  },
  { sequelize, modelName: "user", updatedAt:false, createdAt:false }
);

module.exports = User;
