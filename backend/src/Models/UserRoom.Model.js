const { Model, DataTypes } = require("sequelize");
const Sequelize = require("sequelize");
const path = require("path");
const User = require("./User.Model");
const Room = require("./Room.Model");
const dbPath = path.resolve(__dirname, "chat.sqlite");
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

class UserRoom extends Model {}

UserRoom.init(
  {
    status: {
      type: DataTypes.ENUM('invited', 'accepted', 'declined'),
      defaultValue: 'invited',
      allowNull: false
    },
    // userId: {
    //   type: DataTypes.STRING,
    //   references: { model: User, key: "id" },
    // },
    // roomId: { type: DataTypes.INTEGER, references: { model: Room, key: "id" } },
  },
  { sequelize, modelName: "userRoom" }
);

// User.belongsToMany(Room, { through: UserRoom });
// Room.belongsToMany(User, { through: UserRoom });

UserRoom.belongsTo(Room, { foreignKey: 'roomId' });
UserRoom.belongsTo(User, { foreignKey: 'userId' });


module.exports = UserRoom;
