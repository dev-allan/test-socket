const UserRoom = require("../Models/UserRoom.Model");
const User = require("../Models/User.Model");
const Room = require("../Models/Room.Model");

class UserRoomService {}

UserRoomService.inviteUser = (socket, io) => {
  socket.on("invite_in_room", async (invitation) => {
    const user = await User.findOne({
      where: { id: invitation.userId },
    }).then((res) => {
      return res;
    });
    if (user === null) {
      return console.log("Utilisateur introuvable");
    }
    if (user.dataValues.userId !== socket.decoded.id) {
      UserRoom.create({
        userId: invitation.userId,
        roomId: invitation.roomId,
      })
        .then(() => {
          io.in(invitation.roomId).emit("received_invitation", invitation);
        })
        .catch((e) => {
          console.log(e);
        });
      socket.broadcast.emit("receive_invitation", invitation);
    } else {
      console.log("Vous ne pouvez pas vous déléguer un droit à vous même !");
    }
  });
};

UserRoomService.displayInvitation = (socket, io) => {
  socket.on("display_invitation_room", async (invitation) => {
    const searchUserId = await User.findAll({
      where: { userId: socket.decoded.id },
    }).then((user) => {
      return user[0].dataValues.id;
    });
    await UserRoom.findAll({
      where: { status: "invited", userId: searchUserId },
      include: [{ model: Room }, { model: User }],
    }).then((list) => {
      socket.emit("init_invitations", list);
    });
  });
};

UserRoomService.responseInvitation = (socket, io) => {
  socket.on("response_invitation_room", async (response) => {
    UserRoom.update(
      { status: response.status },
      {
        where: {
          roomId: response.roomId,
        },
      }
    )
      .then(() => {
        console.log("Record updated successfully");
      })
      .catch((err) => {
        console.error(err);
      });
  });
};

module.exports = UserRoomService;
