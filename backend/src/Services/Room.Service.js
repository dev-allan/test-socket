const Room = require("../Models/Room.Model");
const Chat = require("../Models/Chat.Model");

class RoomService {}

RoomService.createRoom = (socket, io) => {
  socket.on("create_room", (data) => {
    console.log(data);
    Room.create({
      createdBy: socket.decoded.id,
      name: data.name,
    })
      .then(() => {
        io.in(data.name).emit("room_created", data);
      })
      .catch((e) => {
        console.log(e);
      });
  });
};

RoomService.roomCreated = (socket, io) => {
  socket.on("room_created", (room) => {
    Room.findAll({
      attributes: ["id", "name"],
      where: {
        createdBy: socket.decoded.id,
      },
    }).then((list) => {
      socket.emit("init_rooms", { rooms: JSON.stringify(list) });
    });
  });
};

RoomService.removeAllRooms = (socket, io) => {
  socket.on("remove_all_rooms", (room) => {
    console.log("removed");
    Room.destroy({
      where: {},
      truncate: true,
    }).then((list) => {
      socket.emit("rooms_removed", { rooms: JSON.stringify(list) });
    });
  });
};

RoomService.enterRoom = (socket, io) => {
  socket.on("enter_room", (room) => {
    //On entre dans la salle demandée
    socket.join(room);
    console.log(socket.rooms);

    // on envoie tous les messages du salon
    Chat.findAll({
      attributes: ["id", "roomId", "message", "createdAt"],
      include: [
        {
          model: Room,
          as: "room",
          attributes: ["id", "name"],
        },
      ],
      where: {
        roomId: room,
      },
    }).then((list) => {
      console.log(JSON.stringify(list));
      socket.emit("init_messages", { messages: JSON.stringify(list) });
    });
  });
};

RoomService.leaveRoom = (socket, io) => {
  socket.on("leave_room", (room) => {
    //On écoute dans les sorties
    socket.leave(room);
    console.log(socket.rooms);
  });
};

module.exports = RoomService;
