const Chat = require("../Models/Chat.Model");

class ChatService {}

ChatService.createMessage = (socket, io) => {
    socket.on("send_message", (msg) => {
        // On stocke le message dans la bdd
        console.log(msg)
        Chat.create({
          name: msg.name,
          roomId: msg.roomId,
          createdBy: socket.decoded.id,
          message: msg.message,
          createdAt: msg.createdAt,
        })
          .then(() => {
            //le message est stocké, on le relaie à tous les utilisateurs dans le salon corresponde
            io.in(msg.roomId).emit("received_message", msg);
          })
          .catch((e) => {
            console.log(e);
          });
        socket.broadcast.emit("receive_message", msg);
      });
}

module.exports = ChatService