const express = require("express");
const app = express();
const http = require("http");
const { Server } = require("socket.io");
const cors = require("cors");
app.use(cors());

const path = require("path");

const server = http.createServer(app);

//on charge sequelize
const Sequelize = require("sequelize");

//On fabrique le lien de la bdd
const dbPath = path.resolve(__dirname, "chat.sqlite");

//on se connecte à la base
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

//on charge les models 
const Chat = require("./src/Models/Chat.Model");
const Room = require("./src/Models/Room.Model");
const User = require("./src/Models/User.Model");
const UserRoom = require("./src/Models/UserRoom.Model")

const { createMessage } = require("./src/Services/Chat.Service");
const {
  createRoom,
  roomCreated,
  removeAllRooms,
  enterRoom,
  leaveRoom,
} = require("./src/Services/Room.Service");

// On effectue le chargement
Chat.sync();
Room.sync();
User.sync();
UserRoom.sync();

const allowedOrigins = [
  "http://localhost:5173",
  "http://localhost:5174",
  "http://localhost:5500",
  "http://localhost:8080",
  "http://localhost:8081",
];

const io = new Server(server, {
  cors: {
    origin: allowedOrigins,
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket) => {
  console.log(`A user connected: ${socket.id}`);

  // partie chat
  createMessage(socket, io);

  // partie salon
  createRoom(socket, io);
  roomCreated(socket, io);
  removeAllRooms(socket, io);
  enterRoom(socket, io);
  leaveRoom(socket, io);
});

server.listen(3000, () => {
  console.log("listening on *:3000");
});
