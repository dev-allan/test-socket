const { Model, DataTypes } = require("sequelize");
const Sequelize = require("sequelize");
const path = require("path");
const User = require("./User.Model");
const Room = require("./Room.Model");
const dbPath = path.resolve(__dirname, "chat.sqlite");
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

class UserRoom extends Model {}

UserRoom.init(
  {
    userId: {
      type: DataTypes.STRING,
      references: { model: User, key: "userId" },
    },
    roomId: { type: DataTypes.INTEGER, references: { model: Room, key: "id" } },
  },
  { sequelize, modelName: "user_room" }
);

User.belongsToMany(Room, { through: UserRoom });
Room.belongsToMany(User, { through: UserRoom });

module.exports = UserRoom;
