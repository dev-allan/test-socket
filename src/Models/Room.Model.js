const { Model, DataTypes } = require("sequelize");
const Chat = require("./Chat.Model");
const Sequelize = require("sequelize");
const path = require("path");
const dbPath = path.resolve(__dirname, "chat.sqlite");
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

class Room extends Model {}

Room.init(
  {
    user_id: { type: DataTypes.STRING },
    name: { type: DataTypes.STRING },
  },
  { sequelize, modelName: "room" }
);

Room.hasMany(Chat, { foreignKey: 'roomId'});
Chat.belongsTo(Room);

module.exports = Room;
