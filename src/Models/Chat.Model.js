const { Model, DataTypes } = require("sequelize");
const Sequelize = require("sequelize");
const path = require("path");
const dbPath = path.resolve(__dirname, "chat.sqlite");
const sequelize = new Sequelize("database", "username", "password", {
  host: "localhost",
  dialect: "sqlite",
  logging: false,
  storage: dbPath,
});

class Chat extends Model {}

Chat.init(
  {
    roomId: { type: DataTypes.NUMBER },
    user_id: { type: DataTypes.INTEGER },
    message: { type: DataTypes.STRING}
  },
  { sequelize, modelName: "chat" }
);

module.exports = Chat