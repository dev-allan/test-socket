import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import io from 'socket.io-client';

const socket = io.connect('http://192.168.1.20:3000', {
  query: {
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzYTFjZmRhMTNlNTQ2Y2ZlNjhlNzU3ZCIsImlhdCI6MTY3MTU0ODkwNiwiZXhwIjoxNjcxNTkyMTA2fQ.YjBCAh8JEoO946jjxq7VbeqERdZjs32Ewt2voXVQcqo',
  },
});

export const Room = () => {
  const [room, setRoom] = useState('');
  const [listRooms, setListRooms] = useState([]);
  const [invitations, setInvitations] = useState([]);

  const navigation = useNavigation();

  const createRoom = () => {
    const data = {name: room};
    socket.emit('create_room', data);
    setRoom('');
    socket.emit('room_created');
  };

  const deleteRoom = () => {
    socket.emit('remove_all_rooms');
    socket.emit('rooms_removed');
    setListRooms([]);
  };

  useEffect(() => {
    socket.emit('room_created');
    socket.on('init_rooms', room => {
      const data = JSON.parse(room.rooms);
      setListRooms(data);
    });
    socket.emit('display_invitation_room');
    socket.on('init_invitations', invit => {
      setInvitations(invit);
    });
  }, [socket]);

  const renderItemRoom = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Chat', {
            roomId: item.id,
          });
        }}
        style={{width: 150, height: 30, backgroundColor: 'blue', margin: 2}}>
        <Text style={{fontSize: 20, color: 'white', margin: 2}}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  const responseInvitation = (status, roomId) => {
    const response = {status: status, roomId: roomId};
    socket.emit('response_invitation_room', response);
    socket.on('init_invitations', invit => {
      setInvitations(JSON.parse(invit));
    });
  };

  const renderItemInvitation = ({item}) => {
    console.log(item);
    return (
      <TouchableOpacity style={{backgroundColor: 'red'}}>
        <Text style={{fontSize: 20, color: 'black', margin: 2}}>
          {item.room.name}
        </Text>
        <Button
          title={'Accepter'}
          onPress={() => responseInvitation('accepted', item.room.id)}
        />
        <Button
          title={'Refuser'}
          onPress={() => responseInvitation('declined', item.room.id)}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1}}>
      <Text style={{color: 'black'}}>Créer un salon</Text>
      <TextInput
        style={{backgroundColord: 'grey', color: 'black'}}
        value={room}
        onChangeText={e => {
          setRoom(e);
        }}
      />
      <Button onPress={() => deleteRoom()} title={'Supprimer'} />
      <Button onPress={() => createRoom()} title={'Créer'} />
      <View style={{flex: 1}}>
        <FlatList
          data={listRooms}
          extraData={listRooms}
          renderItem={renderItemRoom}
        />
      </View>
      <Text>Invitation salon</Text>
        <View style={{flex: 1}}>
          <FlatList
            data={invitations}
            extraData={invitations}
            renderItem={renderItemInvitation}
          />
        </View>
    </View>
  );
};

export default Room;
