import React, {useState, useEffect} from 'react';
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

import io from 'socket.io-client';

const socket = io.connect('http://192.168.1.20:3000', {
  query: {
    token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzYTFjZmRhMTNlNTQ2Y2ZlNjhlNzU3ZCIsImlhdCI6MTY3MTU0ODkwNiwiZXhwIjoxNjcxNTkyMTA2fQ.YjBCAh8JEoO946jjxq7VbeqERdZjs32Ewt2voXVQcqo',
  },
});

export const Chat = ({route, navigation}) => {
  const [message, setMessage] = useState('');
  const [publish, setPublish] = useState([]);
  const [userIdInvit, setUserIdInvit] = useState('')

  const userId = socket.id;

  const {roomId, otherParam} = route.params;

  const sendMessage = () => {
    if (message === '') return;
    const data = {userId, roomId, message, date: Date.now()};
    socket.emit('send_message', data);
    socket.emit('enter_room', roomId);
    socket.on('init_messages', msg => {
      const data = JSON.parse(msg.messages);
      setPublish(data);
    });
    setMessage('');
  };

  const sendInvitation = () => {
    const data = {userId: userIdInvit, roomId: roomId};
    console.log(data)
    socket.emit('invite_in_room', data)
  }

  useEffect(() => {
    socket.emit('enter_room', roomId);
    socket.on('init_messages', msg => {
      const data = JSON.parse(msg.messages);
      setPublish(data);
    });
  }, [socket]);

  const renderItem = ({item}) => {
    return (
      <View>
        <Text>{item.name}</Text>
        <Text style={{color: 'black'}}>{item.message}</Text>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      <Text>Inviter dans salon</Text>
      <TextInput onChangeText={e => {setUserIdInvit(e)}}/>
      <Button onPress={() => sendInvitation()} title={'Inviter'}/>
      <View style={{flex: 1}}>
        <FlatList data={publish} extraData={publish} renderItem={renderItem} />
      </View>
      <TextInput
        onChangeText={e => {
          setMessage(e);
        }}
      />
      <Button onPress={() => sendMessage()} title={'Envoyer'} />
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default Chat;
